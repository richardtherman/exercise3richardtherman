package edu.sjsu.android.exercis3therman;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

public class TextActivity extends AppCompatActivity {
    public final static String EXTRA_TEXT ="edu.sjsu.android.text";
    public void setText(View view){
        Intent replyIntent = new Intent();
        if(view.getId() == R.id.confirm){
            // If confirm button is clicked, set the data as your name
            replyIntent.putExtra(EXTRA_TEXT, "Your Name");
            // And set the activity's result to RESULT_OK
            setResult(RESULT_OK, replyIntent);
            } else if (view.getId() == R.id.cancel){
             setResult(RESULT_CANCELED, replyIntent);     }
        finish(); }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_text);
    }
}